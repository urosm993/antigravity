﻿using UnityEngine;

public class ScorePreservation : MonoBehaviour {

    public static int[] levelsScore = new int[30];
    public static int[] levelsProgress = new int[30];
    public static float[] levelsDifficulty = new float[30];
    public static int maximumLevel = 1;
    public static int totalLuminas = 0;
    public static int addScore = 0;
    public static int totalScore = 0;
    public static int currentScore = 0;
    public static int collectedLuminas = 0;
    public static int currentProgress = 0;
    public static int bestScore = 0;
    public static int level = -1;
    public static float playerId;
    public static readonly int DIFF_BONUS = 11;

    public static void UpdateScore(int score, int progress, int luminas) {

        int scoreFactored = score;
        if (level > -1) {
            if (progress >= 99) {
                scoreFactored = (int)(score + (score / DIFF_BONUS) * levelsDifficulty[level]);
                if (level + 1 > maximumLevel) {
                    maximumLevel = level + 1;
                    addScore = scoreFactored;
                }
            }
            currentScore = scoreFactored;
            currentProgress = progress;
            collectedLuminas = luminas;
            if (scoreFactored > levelsScore[level]) {
                levelsScore[level] = scoreFactored;
            }
            if (progress > levelsProgress[level]) {
                levelsProgress[level] = progress;
            }
        } else {
            //Progress is bestScore!
            collectedLuminas = luminas;
            bestScore = progress;
            currentScore = scoreFactored;
            if (currentProgress > bestScore) {
                bestScore = currentScore;
            }
        }
    }

}
