﻿using UnityEngine;
using UnityEngine.UI;

public class CubbieData : MonoBehaviour {
    public Text agValueBox;
    public Text agTextBox;
    public Text dstBox;
    public Text scoreBox;
    public Text bestTxt;
    public Text message;
    public Text warning;

    public Text luminasBox;
    public Text luminasTxt;

    public static int shield;
    public static int antigravity;
    public GameObject lightObj;
    public GameObject shieldObj;
    public Transform cam;

    private Renderer shieldRenderer;
    private Light myLight;
    private int levelLen;
    private int luminas;
    private float score;
    private int progress;
    private int lvl;

    private int bestScore;
    private Rigidbody rb;
    private readonly int BOTTOM = -30;
    private float lastZ;
    private bool invulnerable = false;
    private bool dimming = false;

    // Use this for initialization
    void Start() {
        shieldRenderer = shieldObj.GetComponent<Renderer>();
        antigravity = 1;
        lvl = ScorePreservation.level;
        //Endless
        if (lvl == -1) {
            bestScore = ScorePreservation.bestScore;
        } else {
            if (lvl == 0) {
                message.text = "Simply tap left or right part of the screen.\nThe longer you hold the more force you apply.";
                transform.position = new Vector3(transform.position.x, transform.position.y, -200f);
                cam.position = new Vector3(cam.position.x, cam.position.y, -197f);
            }
            if (lvl == 5 || lvl == 11 || lvl == 17 || lvl == 23 || lvl == 29) {
                myLight = lightObj.GetComponent<Light>();
                luminas = ScorePreservation.totalLuminas;
                luminasTxt.text = "Luminas left:";
                luminasBox.text = "" + luminas;
                InvokeRepeating("LuminasLost", 2f, 2f);
            }
            levelLen = GameObject.Find("Game").GetComponent<LevelSetup>().GetTotalLevelLen();
        }
        rb = GetComponent<Rigidbody>();
        InvokeRepeating("SetScore", 1f, 0.2f);

        Color c = shieldRenderer.material.color;
        c.a = 0.0f;
        shieldRenderer.material.color = c;

        if (lvl == -1) {
            if (PlayerUnlocks.startShield == 1) {
                if (PlayerUnlocks.superShield == 0) {
                    ActivateHalfShield();
                } else {
                    ActivateShield();
                }
            }
        }
    }

    void FixedUpdate() {
        if (lvl == -1) {
            if (transform.position.y < BOTTOM) {
                gameObject.SetActive(false);
                GameObject.Find("GameEndless").GetComponent<EndGame>().GameOver();
                EnableAntigravity();
                transform.position = new Vector3(0, 0, score * 5f - 80);
                warning.text = "";
            }
        }
        if (antigravity <= 0) {
            agTextBox.text = "WARNING:";
            agValueBox.text = "";
            warning.text = "Cannot resist gravity";
        } else {
            agTextBox.text = "Antigravity shield";
            if (shield == 2) {
                agValueBox.text = "Strong";
            } else if (shield == 1) {
                agValueBox.text = "Weakened";
            } else {
                agValueBox.text = "Broken";
            }
            if (lvl == -1) {
                ScorePreservation.UpdateScore((int)score, bestScore, luminas);
            } else {
                progress = (int)rb.position.z * 100 / levelLen;
                ScorePreservation.UpdateScore((int)score, progress, luminas);
            }

        }

        Collider[] hitColliders = Physics.OverlapSphere(transform.position, 12);
        int i = 0;
        //if (lvl != 28) {
        while (i < hitColliders.Length) {
            if (hitColliders[i].gameObject.name.Contains("Attractive") && antigravity > 0) {
                rb.AddForce((hitColliders[i].gameObject.transform.position - transform.position).normalized * (12f / 30f + Vector2.Distance(new Vector3(hitColliders[i].gameObject.transform.position.x, 0, hitColliders[i].gameObject.transform.position.z), transform.position)), ForceMode.Impulse);
            } else if (hitColliders[i].gameObject.name.Contains("Repellent") && antigravity > 0) {
                rb.AddForce(-(hitColliders[i].gameObject.transform.position - transform.position).normalized * (12f / 30f + Vector2.Distance(new Vector3(hitColliders[i].gameObject.transform.position.x, 0, hitColliders[i].gameObject.transform.position.z), transform.position)), ForceMode.Impulse);
            }
            i++;
            //}
        }
    }

    void EnableAntigravity() {
        rb.constraints = RigidbodyConstraints.FreezePositionY;
        rb.useGravity = false;
        gameObject.GetComponent<BoxCollider>().enabled = true;
        antigravity = 1;
    }

    void DisableAntigravity() {
        rb.constraints = RigidbodyConstraints.None;
        rb.useGravity = true;
        gameObject.GetComponent<BoxCollider>().enabled = false;
    }

    void LuminasLost() {
        if (luminas > 0) {
            luminas--;
        } else {
            if (dimming == false) {
                dimming = true;
                InvokeRepeating("DimLight", 2f, 5f);
            }
        }
    }

    void DimLight() {
        myLight.intensity--;
        myLight.intensity--;
    }

    private void ActivateShield() {
        Color c = shieldRenderer.material.color;
        c.a = 0.5f;
        shieldRenderer.material.color = c;
        shield = 2;
    }

    private void ActivateHalfShield() {
        Color c = shieldRenderer.material.color;
        c.a = 0.2f;
        shieldRenderer.material.color = c;
        shield = 1;
    }

    void OnCollisionEnter(Collision other) {
        if (other.gameObject.CompareTag("Lumina")) {
            other.gameObject.GetComponent<Lumina>().Explode();
            other.gameObject.SetActive(false);
            luminas++;
            if (lvl == -1 && PlayerUnlocks.luminasTimes2 == 1) {
                luminas++;
            }
            score += 5;
            if (lvl == -1 && PlayerUnlocks.luminasWorthMore == 1) {
                score += 4;
            }
        }
        if (other.gameObject.CompareTag("Collectable")) {
            if (shield == 2) {
                score += 9;
            }
            if (shield == 1) {
                score += 3;
            }
            if (lvl == -1 && PlayerUnlocks.superShield == 0) {
                ActivateHalfShield();
            } else {
                ActivateShield();
            }
            other.gameObject.GetComponent<Explosion>().Explode();
            other.gameObject.SetActive(false);
            SetScore();
        }
        if (other.gameObject.CompareTag("Obsticle") && !invulnerable) {
            invulnerable = true;
            Color c = shieldRenderer.material.color;
            c.a -= 0.3f;
            shieldRenderer.material.color = c;
            rb.AddForce(-(other.gameObject.transform.position - transform.position).normalized * 300f, ForceMode.Impulse);
            if (shield > 0) {
                shield--;
                gameObject.GetComponent<Explosion>().Explode();
                other.gameObject.GetComponent<Collider>().enabled = false;
            } else {
                antigravity = 0;
                gameObject.GetComponent<Collider>().enabled = false;
                Invoke("DisableAntigravity", 0.1f);
                gameObject.GetComponent<Explosion>().DeathExplode();

            }
            Invoke("BecomeVulnerable", 1f);
        }
    }

    void BecomeVulnerable() {
        invulnerable = false;
    }

    void SetScore() {
        if (lvl == -1) {
            if (rb.position.z > -5f) {
                if (message.text != "") {
                    message.text = "";
                }
                score += ((rb.position.z - lastZ) / 7f);
                lastZ = rb.position.z;
            }
            if (score > bestScore) {
                bestScore = (int)score;
            }
            if (bestTxt != null) {
                bestTxt.text = "" + bestScore;
            }
            scoreBox.text = "" + (int)score;
            luminasTxt.text = "Luminas:";
            luminasBox.text = "" + luminas;
        } else {
            if (rb.position.z > -5f) {
                if (message.text != "") {
                    message.text = "";
                }
                score += ((rb.position.z - lastZ) / 7f);
                lastZ = rb.position.z;

                dstBox.text = "" + progress + "%";
                if (ScorePreservation.level >= 2) {
                    if (lvl == 5 || lvl == 11 || lvl == 17 || lvl == 23 || lvl == 29) {
                        luminasTxt.text = "Luminas left:";
                        luminasBox.text = "" + luminas;
                    } else {
                        luminasTxt.text = "Luminas:";
                        luminasBox.text = "" + luminas;
                    }
                }
            }
            scoreBox.text = "" + (int)score;
        }
    }
}
