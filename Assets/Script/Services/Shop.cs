﻿using UnityEngine;
using UnityEngine.UI;

public class Shop : MonoBehaviour {
    public Button[] cubieCosmetics;
    public Button[] shields;

    public Text[] prices;
    public Text message;
    public GameObject menuCubie;

    void Start() {
        if (PlayerUnlocks.goldenCubie == 1) {
            prices[1].text = 0 + "";
        }
        if (PlayerUnlocks.iceCubie == 1) {
            prices[2].text = 0 + "";
        }
        if (PlayerUnlocks.goldenShield == 1) {
            prices[4].text = 0 + "";
        }
        UpdateChanges();
    }
    private void NotEnoughLuminas() {
        message.text = "Not enough luminas. Collect more, or try watching a video to get some";
        Invoke("ResetMessage", 4f);
    }

    private void ResetMessage() {
        message.text = "";
    }

    public void GoldenCubie() {
        if (PlayerUnlocks.goldenCubie == 0) {
            int price = 100;
            if (ScorePreservation.totalLuminas >= price) {
                UpdateLuminas(price);
                PlayerUnlocks.goldenCubie = 1;
                prices[1].text = 0 + "";
                ActivateCubie(1);
                UpdateChanges();
            } else {
                NotEnoughLuminas();
            }
        } else {
            ActivateCubie(1);
        }
    }

    public void IceCubie() {
        if (PlayerUnlocks.iceCubie == 0) {
            int price = 200;
            if (ScorePreservation.totalLuminas >= price) {
                UpdateLuminas(price);
                PlayerUnlocks.iceCubie = 1;
                prices[2].text = 0 + "";
                ActivateCubie(2);
                UpdateChanges();
            } else {
                NotEnoughLuminas();
            }
        } else {
            ActivateCubie(2);
        }
    }

    public void ActivateDefaultCubie() {
        PlayerUnlocks.cosmeticsSelected = 0;
        UpdateChanges();
    }

    public void GoldenShield() {
        if (PlayerUnlocks.goldenShield == 0) {
            int price = 125;
            if (ScorePreservation.totalLuminas >= price) {
                UpdateLuminas(price);
                PlayerUnlocks.goldenShield = 1;
                prices[4].text = 0 + "";
                ActivateShield(1);
                UpdateChanges();
            } else {
                NotEnoughLuminas();
            }
        } else {
            ActivateShield(1);
        }
    }

    public void ActivateDefaultShield() {
        PlayerUnlocks.shieldSelected = 0;
        UpdateChanges();
    }

    void UpdateLuminas(int deduce) {
        ScorePreservation.totalLuminas = PlayerPrefs.GetInt("luminas") - deduce;
        PlayerPrefs.SetInt("luminas", ScorePreservation.totalLuminas);
    }

    void SavePrefs() {
        PlayerPrefs.SetInt("goldenCubie", PlayerUnlocks.goldenCubie);
        PlayerPrefs.SetInt("iceCubie", PlayerUnlocks.iceCubie);
        PlayerPrefs.SetInt("goldenShield", PlayerUnlocks.goldenShield);
        PlayerPrefs.SetInt("cosmeticsSelected", PlayerUnlocks.cosmeticsSelected);
        PlayerPrefs.SetInt("shieldSelected", PlayerUnlocks.shieldSelected);
    }

    private void ActivateCubie(int code) {
        PlayerUnlocks.cosmeticsSelected = code;
        GameObject shield = menuCubie.transform.GetChild(0).gameObject;
        shield.SetActive(false);
        UpdateChanges();
    }

    private void ActivateShield(int code) {
        PlayerUnlocks.shieldSelected = code;
        GameObject shield = menuCubie.transform.GetChild(0).gameObject;
        shield.SetActive(true);
        UpdateChanges();
    }

    void UpdateChanges() {
        foreach (Button b in cubieCosmetics) {
            b.interactable = true;
        }
        cubieCosmetics[PlayerUnlocks.cosmeticsSelected].interactable = false;

        foreach (Button b in shields) {
            b.interactable = true;
        }
        shields[PlayerUnlocks.shieldSelected].interactable = false;
        menuCubie.GetComponent<CubieSwapMaterial>().Swap();
        SavePrefs();
    }
}
