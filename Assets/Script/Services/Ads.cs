﻿
using UnityEngine;
using UnityEngine.Advertisements;

public class Ads {

    private static Ads ads;
    private readonly int AD_BONUS = 25;

    private Ads() {
    }

    public static Ads getAds() {
        if (ads == null) {
            ads = new Ads();
        }
        if (!Advertisement.isInitialized) {
            Advertisement.Initialize("3412184");
        }
        return ads;
    }
    public void PlayAd() {
        ShowOptions so = new ShowOptions {
            resultCallback = AdReward
        };
        Advertisement.Show("rewardedVideo", so);
    }

    public bool IsReady() {
        return Advertisement.IsReady();
    }

    public void PlayShortAd() {
        Advertisement.Show("video");
    }

    private void AdReward(ShowResult sr) {
        if (sr == ShowResult.Finished) {
            ScorePreservation.totalLuminas = PlayerPrefs.GetInt("luminas") + AD_BONUS;
            PlayerPrefs.SetInt("luminas", ScorePreservation.totalLuminas);
        }
    }
}
