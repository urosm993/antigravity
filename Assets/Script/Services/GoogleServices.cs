﻿using UnityEngine;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.UI;
/*
public class GoogleServices : MonoBehaviour {
    public Text info;
    public Button story;
    public Button endless;
    public static PlayGamesPlatform platform;
    private void Start() {
        ConnectToPlayGames();
        InvokeRepeating("CheckConnection", 10f, 10f);
    }

    private void ConnectToPlayGames() {
        if (Internet.Connected()) {
            if (platform == null) {
                PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().Build();
                PlayGamesPlatform.InitializeInstance(config);
                platform = PlayGamesPlatform.Activate();
            }
            Authenticate();
        } else {
            Invoke("ConnectToPlayGames", 10f);
        }
    }

    private void Authenticate() {
        if (!Social.Active.localUser.authenticated) {
            Social.Active.localUser.Authenticate((bool success, string err) => {
                if (success) {
                    info.text = "Successfully authenticated";
                    Invoke("ClearMessage", 3f);

                } else {
                    info.text = err;
                    Invoke("ClearMessage", 3f);
                    Invoke("Authenticate", 10f);
                }
            });
        }
    }

    private void CheckConnection() {
        if (Internet.Connected()) {
            story.interactable = true;
            endless.interactable = true;
        } else {
            story.interactable = false;
            endless.interactable = false;
            info.text = "Please check your internet connection";
            Invoke("ClearMessage", 3f);
        }
    }

    private void ClearMessage() {
        info.text = "";
    }
   
    public static void CheckForAchievements(int levelEnded) {
        if (levelEnded >= 0) {
            Social.ReportProgress(GPGSIds.achievement_training, 100.0f, (bool success) => { });
        }
        if (levelEnded >= 5) {
            Social.ReportProgress(GPGSIds.achievement_prologue, 100.0f, (bool success) => { });
        }
        if (levelEnded >= 11) {
            Social.ReportProgress(GPGSIds.achievement_expectation, 100.0f, (bool success) => { });
        }
        if (levelEnded >= 17) {
            Social.ReportProgress(GPGSIds.achievement_inconvenience, 100.0f, (bool success) => { });
        }
        if (levelEnded >= 23) {
            Social.ReportProgress(GPGSIds.achievement_risk, 100.0f, (bool success) => { });
        }
        if (levelEnded >= 29) {
            Social.ReportProgress(GPGSIds.achievement_worth, 100.0f, (bool success) => { });
        }
        Social.ReportProgress(GPGSIds.achievement_story_mode_completed, ((levelEnded + 1) / 30f) * 100f, (bool success) => { });
    }

    public static void UpdateScoreEndless(long score) {
        Social.ReportScore(score, GPGSIds.leaderboard_endless, (bool success) => { });
    }

    public static void UpdateScoreStory(long score) {
        if (score > 50000) {
            Social.ReportScore(10000, GPGSIds.leaderboard_story, (bool success) => { });
        } else {
            Social.ReportScore(score, GPGSIds.leaderboard_story, (bool success) => { });
        }
    }
  
    public void ShowLeaderboard() {
        Social.ShowLeaderboardUI();
    }

    public void ShowAchievements() {
        Social.ShowAchievementsUI();
    }
}
*/