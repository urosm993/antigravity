﻿using UnityEngine;

public class PlayerUnlocks : MonoBehaviour {
    public static int startShield;
    public static int superShield;
    public static int spawnShields;
    public static int repair;
    public static int luminasTimes2;
    public static int luminasWorthMore;

    public static int cosmeticsSelected;
    public static int shieldSelected;

    public static int goldenCubie;
    public static int iceCubie;

    public static int goldenShield;
    public static int thunderShield;

}
