﻿using UnityEngine;
using UnityEngine.UI;

public class Upgrades : MonoBehaviour {
    public Button startShieldB;
    public Button superShieldB;
    public Button spawnShieldsB;
    public Button repairB;
    public Button luminasTimes2B;
    public Button luminasWorthMoreB;
    public Text message;

    void Start() {
        startShieldB.interactable = PlayerUnlocks.startShield == 0 ? true : false;
        superShieldB.interactable = PlayerUnlocks.superShield == 0 ? true : false;
        spawnShieldsB.interactable = PlayerUnlocks.spawnShields == 0 ? true : false;
        repairB.interactable = PlayerUnlocks.repair == 0 ? true : false;
        luminasTimes2B.interactable = PlayerUnlocks.luminasTimes2 == 0 ? true : false;
        luminasWorthMoreB.interactable = PlayerUnlocks.luminasWorthMore == 0 ? true : false;

        repairB.interactable = false;
    }

    private void NotEnoughLuminas() {
        message.text = "Not enough luminas. Try watching a video to get more";
        Invoke("ResetMessage", 4f);
    }

    private void ResetMessage() {
        message.text = "";
    }

    public void StartShield() {
        int price = 75;
        if (ScorePreservation.totalLuminas >= price) {
            UpdateLuminas(price);
            PlayerUnlocks.startShield = 1;
            startShieldB.interactable = false;
            SavePrefs();
        } else {
            NotEnoughLuminas();
        }
    }
    public void SuperShield() {
        int price = 270;
        if (ScorePreservation.totalLuminas >= price) {
            UpdateLuminas(price);
            PlayerUnlocks.superShield = 1;
            superShieldB.interactable = false;
            SavePrefs();
        } else {
            NotEnoughLuminas();
        }
    }
    public void SpawnShields() {
        int price = 550;
        if (ScorePreservation.totalLuminas >= price) {
            UpdateLuminas(price);
            PlayerUnlocks.spawnShields = 1;
            spawnShieldsB.interactable = false;
            SavePrefs();
        } else {
            NotEnoughLuminas();
        }
    }
    public void LuminasTimes2() {
        int price = 420;
        if (ScorePreservation.totalLuminas >= price) {
            UpdateLuminas(price);
            PlayerUnlocks.luminasTimes2 = 1;
            luminasTimes2B.interactable = false;
            SavePrefs();
        } else {
            NotEnoughLuminas();
        }
    }
    public void LuminasWorthMore() {
        int price = 110;
        if (ScorePreservation.totalLuminas >= price) {
            UpdateLuminas(price);
            PlayerUnlocks.luminasWorthMore = 1;
            luminasWorthMoreB.interactable = false;
            SavePrefs();
        } else {
            NotEnoughLuminas();
        }
    }
    public void Repair() {
        int price = 325;
        if (ScorePreservation.totalLuminas >= price) {
            UpdateLuminas(price);
            PlayerUnlocks.repair = 1;
            repairB.interactable = false;
            SavePrefs();
        } else {
            NotEnoughLuminas();
        }
    }

    void UpdateLuminas(int deduce) {
        ScorePreservation.totalLuminas = PlayerPrefs.GetInt("luminas") - deduce;
        PlayerPrefs.SetInt("luminas", ScorePreservation.totalLuminas);
    }

    void SavePrefs() {
        PlayerPrefs.SetInt("startShield", PlayerUnlocks.startShield);
        PlayerPrefs.SetInt("superShield", PlayerUnlocks.superShield);
        PlayerPrefs.SetInt("spawnShields", PlayerUnlocks.spawnShields);
        PlayerPrefs.SetInt("repair", PlayerUnlocks.repair);
        PlayerPrefs.SetInt("luminasTimes2", PlayerUnlocks.luminasTimes2);
        PlayerPrefs.SetInt("luminasWorthMore", PlayerUnlocks.luminasWorthMore);
    }
}
