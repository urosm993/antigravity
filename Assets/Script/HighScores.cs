﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class HighScores : MonoBehaviour {

    public Text textList;

    public static readonly string privateCode = "5PqGfH1K006F2rjV2wazvAEnuxG6D1W0Wh6f2mcWDWwQ";
    public static readonly string webURL = "http://dreamlo.com/lb/";
    const string publicCode = "5e0cf1d9fe224b04787d9cad";

    public HighScore[] scoreList;

    void Awake() {
        InvokeRepeating("FetchScores", 1f, 30f);
    }

    public void RefreshScore() {
        if (scoreList == null) return;
        textList.text = "";
        int N = scoreList.Length < 10 ? scoreList.Length : 10;
        for (int i = 0; i < N; i++) {
            if (scoreList[i].name == "Player" + ScorePreservation.playerId) {
                textList.text += i + 1 + ": YOU" + "..............." + scoreList[i].score + '\n';
            } else {
                textList.text += i + 1 + ": " + scoreList[i].name + ".........." + scoreList[i].score + '\n';
            }
        }
    }

    public void FetchScores() {
        StartCoroutine(DownloadScores());
    }
    IEnumerator DownloadScores() {
        UnityWebRequest www = UnityWebRequest.Get(webURL + privateCode + "/pipe/");
        yield return www.SendWebRequest();
        if (www.isNetworkError || www.isHttpError) {
            textList.text = www.error;
        } else {
            FormatScore(www.downloadHandler.text);
            RefreshScore();
        }
    }

    void FormatScore(string textStream) {
        string[] entries = textStream.Split(new char[] { '\n' }, System.StringSplitOptions.RemoveEmptyEntries);
        scoreList = new HighScore[entries.Length];

        for (int i = 0; i < entries.Length; i++) {
            string[] entryInfo = entries[i].Split(new char[] { '|' });
            string name = entryInfo[0];
            int score = int.Parse(entryInfo[1]);
            scoreList[i] = new HighScore(name, score);
        }
    }
}

public struct HighScore {
    public string name;
    public int score;
    public HighScore(string name, int score) {
        this.name = name;
        this.score = score;
    }
}
