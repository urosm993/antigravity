﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OG : MonoBehaviour {

    public Transform cubieEndless;
    public Transform obstacle;
    public Transform obstacleSway;
    public Transform attractive;
    public Transform repellent;
    //public Transform meteor;

    private static Transform sObstacle;
    private static Transform sObstacleSway;
    private static Transform sAttractive;
    private static Transform sRepellent;
    //private static Transform sMeteor;

    // Obstacle generator
    private static Queue<Transform> pool;
    private static List<Transform> used;
    private static readonly int totalObstacles = 100;

    void Awake() {
        Random.InitState(System.DateTime.Now.Millisecond);
        pool = new Queue<Transform>();
        used = new List<Transform>();
        sObstacle = obstacle;
        sObstacleSway = obstacleSway;
        sAttractive = attractive;
        sRepellent = repellent;
        //sMeteor = meteor;

        //Initial Spawn
        for (int i = 0; i < totalObstacles; i++) {
            Transform o = Instantiate(obstacle, new Vector3(0, -97, 0), Quaternion.identity);
            o.gameObject.SetActive(false);
            pool.Enqueue(o);
        }
        InvokeRepeating("CleanUp", 1f, 0.2f);
    }

    public void CleanUp() {
        List<Transform> stillInUse = new List<Transform>();
        foreach (Transform u in used) {
            if (u.transform.position.z + 20 > cubieEndless.position.z) {
                stillInUse.Add(u);
            } else {
                u.gameObject.SetActive(false);
                pool.Enqueue(u);
            }
        }
        used.Clear();
        used = stillInUse;
    }

    private static void InstantiateMore(int cnt) {
        int rnd = Random.Range(0, 100);
        for (int i = 0; i < cnt; i++) {
            Transform pref;
            if (rnd < 15) {
                pref = sObstacleSway;
            } else if (rnd >= 15 && rnd < 25) {
                pref = sAttractive;
            } else if (rnd >= 25 && rnd < 35) {
                pref = sRepellent;
            } else {
                pref = sObstacle;
            }
            Transform o = Instantiate(pref, new Vector3(0, -97, 0), Quaternion.identity);
            o.gameObject.SetActive(false);
            pool.Enqueue(o);
        }
    }

    public static Transform Get() {
        if (pool.Count == 0) {
            InstantiateMore(totalObstacles);
        }
        Transform ret = pool.Dequeue();
        ret.gameObject.SetActive(true);
        ret.gameObject.GetComponent<Collider>().enabled = true;
        used.Add(ret);
        return ret;
    }

}


