﻿using UnityEngine;

public class EndlessSpawn : MonoBehaviour {
    public Transform playerTransform;
    private readonly int WIDTH = 12;
    private int spawnNo = 30;
    private readonly int INC = 5;
    private int trigger;

    public Transform collectable;
    public Transform lumina;

    void Start() {
        Physics.gravity = new Vector3(0, -100, 0);
        trigger = 0;
    }

    // Update is called once per frame
    void FixedUpdate() {
        if (playerTransform.position.z > trigger) {
            trigger += 350;
            Spawn();
        }
        RestrainXPosition();
    }

    private void Spawn() {
        Random.InitState(System.DateTime.Now.Millisecond);
        Transform obs;
        int failed = 0;
        int j = 0;

        Instantiate(lumina, new Vector3(Random.Range(-WIDTH, WIDTH), 0, playerTransform.position.z + Random.Range(0, 300) + 50), Quaternion.identity);
        if (PlayerUnlocks.spawnShields == 1 && Random.Range(0, 100) < 50) {
            Instantiate(collectable, new Vector3(Random.Range(-WIDTH, WIDTH), 0, playerTransform.position.z + Random.Range(0, 300) + 50), Quaternion.identity);
        }
        while (j < spawnNo) {
            float rndWidth = Random.Range(-WIDTH, WIDTH);
            float rndZpos = playerTransform.position.z + Random.Range(0, 300) + 50;
            if (Physics.CheckSphere(new Vector3(rndWidth, 0, rndZpos), 8)) {
                failed++;
                if (failed > 200) {
                    failed = 0;
                    j++;
                }
            } else {
                obs = OG.Get();
                obs.position = new Vector3(rndWidth, -97, rndZpos);
                j++;
            }
        }
        spawnNo += INC;
    }

    void RestrainXPosition() {
        if (playerTransform.position.x > WIDTH) {
            playerTransform.position = new Vector3(WIDTH, playerTransform.position.y, playerTransform.position.z);
        }
        if (playerTransform.position.x < -WIDTH) {
            playerTransform.position = new Vector3(-WIDTH, playerTransform.position.y, playerTransform.position.z);
        }
    }

}
