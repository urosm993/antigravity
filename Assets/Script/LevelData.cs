﻿
public class LevelData {
    //obsticles 40 segment 5

    public LevelData(int lvl, int obstacles = 30, int meteorsFeq = 0, int att = 0, int repel = 0, int segments = 4, int luminas = 1, int increase = 5, int collectables = 1, int segLen = 500, int bufferZone = 100, float rockBottom = -30, float widthLimit = 12) {
        Obstacles = obstacles;
        Segments = segments;
        Luminas = luminas;
        if (obstacles == 0) {
            Increase = 0;
        } else {
            Increase = increase;
        }
        Collectables = collectables;
        SegLen = segLen;
        Att = att;
        Repel = repel;
        BufferZone = bufferZone;
        RockBottom = rockBottom;
        WidthLimit = widthLimit;
        MeteorsFeq = meteorsFeq;
        Lvl = lvl;

        if (lvl == 0 || lvl == 1 || lvl == 5 || lvl == 11 || lvl == 17 || lvl == 23 || lvl == 29) {
            Luminas = 0;
        }
    }

    public int Obstacles {
        get;
        set;
    }

    public int Segments {
        get;
        set;
    }

    public int Luminas {
        get;
        set;
    }

    public int Increase {
        get;
        set;
    }

    public int Collectables {
        get;
        set;
    }

    public int SegLen {
        get;
        set;
    }
    public int Att {
        get;
        set;
    }
    public int Repel {
        get;
        set;
    }
    public int BufferZone {
        get;
        set;
    }

    public float RockBottom {
        get;
        set;
    }

    public float WidthLimit {
        get;
        set;
    }
    public int MeteorsFeq {
        get;
        set;
    }
    public int Lvl {
        get;
        set;
    }

    public int GetTotalLevelLen() {
        return SegLen * Segments;
    }
}
