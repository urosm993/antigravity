﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meteor : MonoBehaviour {
    private Rigidbody rb;
    private float speed;
    private float rotation;
    void Start() {
        speed = Random.Range(50, 200);
        rotation = Random.Range(0.5f, 2f);
        rb = GetComponent<Rigidbody>();
        rb.maxAngularVelocity = 10;
    }
    // Update is called once per frame
    void FixedUpdate() {
        rb.AddForce(new Vector3(-speed, 0, -speed * Random.Range(0.5f, 6f)));
        rb.AddTorque(new Vector3(rotation, 2 * rotation, 3 * rotation));
    }

}
