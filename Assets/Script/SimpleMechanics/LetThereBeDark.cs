﻿using UnityEngine;

public class LetThereBeDark : MonoBehaviour {
    // Start is called before the first frame update
    void Start() {
        RenderSettings.ambientLight = Color.black;
    }
}
