﻿using UnityEngine;

public class Lumina : MonoBehaviour {
    private GameObject player;
    private Rigidbody rb;
    private float pushTimerH;
    private float pushTimerV;
    private float pushForceH;
    private float pushForceV;

    public GameObject particles;

    // Start is called before the first frame update
    void Start() {
        player = GameObject.FindGameObjectsWithTag("Player")[0];
        rb = GetComponent<Rigidbody>();
        Random.InitState(System.DateTime.Now.Millisecond);
        pushTimerH = 2f;
        pushTimerV = 2f;
        pushForceH = Random.Range(-0.8f, 0.8f);
        pushForceV = Random.Range(-1.2f, 3f);
    }

    public void Explode() {
        Instantiate(particles, transform.position, transform.rotation);
    }

    // Update is called once per frame
    void FixedUpdate() {
        rb.AddForce(new Vector3(pushForceH, 0, pushForceV));
        pushTimerH -= Time.deltaTime;
        pushTimerV -= Time.deltaTime;
        if (pushTimerH < 0) {
            pushTimerH = Random.Range(0.5f, 2f);
            pushForceH = Random.Range(-0.8f, 0.8f);
        }
        if (pushTimerV < 0) {
            pushTimerV = Random.Range(1f, 3f);
            pushForceV = Random.Range(-1.2f, 3f);
        }
        Restrain();
    }

    void Restrain() {
        if (rb.position.x > 13f) {
            rb.position = new Vector3(-13, rb.position.y, rb.position.z + 10);
        }
        if (rb.position.x < -13f) {
            rb.position = new Vector3(13, rb.position.y, rb.position.z + 10);
        }

        if (transform.position.z + 20 < player.transform.position.z) {
            transform.position = new Vector3(transform.position.x, transform.position.y, transform.transform.position.z + 100);
        }
    }
}
