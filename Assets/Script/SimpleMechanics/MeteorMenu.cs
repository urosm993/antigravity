﻿using UnityEngine;

public class MeteorMenu : MonoBehaviour {
    private Rigidbody rb;
    private float speed;
    // Use this for initialization
    void Start() {
        speed = Random.Range(200, 500);
        rb = GetComponent<Rigidbody>();
        rb.maxAngularVelocity = 10;
    }
    // Update is called once per frame
    void FixedUpdate() {
        rb.AddForce(new Vector3(-speed, speed, 0));
        rb.AddTorque(new Vector3(speed / 50, speed / 35, speed / 20));
        if (transform.position.x < -50) {
            transform.position = new Vector3(50, Random.Range(-120, 0), 70);
            speed = Random.Range(30, 500);
        }
    }

}
