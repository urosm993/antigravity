﻿using UnityEngine;

public class CubieSwapMaterial : MonoBehaviour {
    public Material cyan;
    public Material golden;
    public Material ice;
    public Material hex;
    public Material hexGold;

    void Start() {
        Swap();
    }

    public void Swap() {
        int cubie = PlayerUnlocks.cosmeticsSelected;
        int shield = PlayerUnlocks.shieldSelected;
        if (cubie == 0) {
            GetComponent<MeshRenderer>().material = cyan;
            transform.GetChild(1).GetComponent<ParticleSystem>().startColor = new Color(0.15f, 0.95f, 0.95f);
        } else if (cubie == 1) {
            GetComponent<MeshRenderer>().material = golden;
            transform.GetChild(1).GetComponent<ParticleSystem>().startColor = new Color(1f, 0.8431373f, 0f);
        } else if (cubie == 2) {
            GetComponent<MeshRenderer>().material = ice;
            transform.GetChild(1).GetComponent<ParticleSystem>().startColor = new Color(0.15f, 0.95f, 0.95f);
        }

        if (shield == 0) {
            transform.GetChild(0).GetComponent<MeshRenderer>().material = hex;
        } else if (shield == 1) {
            transform.GetChild(0).GetComponent<MeshRenderer>().material = hexGold;
        }
    }
}
