﻿using UnityEngine;

public class Explosion : MonoBehaviour {

    public GameObject explosion;
    public GameObject deathExplosion;

    void Start() {

    }

    public void Explode() {
        Instantiate(explosion, transform.position, transform.rotation);
    }

    public void DeathExplode() {
        if (PlayerUnlocks.cosmeticsSelected == 1) {
            deathExplosion.GetComponent<ParticleSystem>().startColor = Colors.golden;
        } else {
            deathExplosion.GetComponent<ParticleSystem>().startColor = Colors.cyan;
        }
        Instantiate(deathExplosion, transform.position, transform.rotation);
    }
}
