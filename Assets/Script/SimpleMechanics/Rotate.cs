﻿using UnityEngine;

public class Rotate : MonoBehaviour {

    private Rigidbody rb;
    // Use this for initialization
    void Start() {
        rb = gameObject.GetComponent<Rigidbody>();
        rb.maxAngularVelocity = 20;
        rb.AddTorque(250, 0, 80, ForceMode.Impulse);
    }

    // Update is called once per frame
    void FixedUpdate() {
        rb.AddTorque(250, 0, 80, ForceMode.Impulse);
    }
}
