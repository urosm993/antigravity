﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubieMenu : MonoBehaviour {

    private Rigidbody rb;
    public Rigidbody shieldRb;
    public float MAX_SPEED;
    public float MAX_ROTATION;

    void Start() {
        rb = GetComponent<Rigidbody>();
        rb.AddTorque(new Vector3(MAX_ROTATION, 2 * MAX_ROTATION, 3 * MAX_ROTATION));
        rb.maxAngularVelocity = 10;
    }

    void FixedUpdate() {
        rb.AddForce(new Vector3(-MAX_SPEED, MAX_SPEED));
        rb.AddTorque(new Vector3(MAX_ROTATION, 2 * MAX_ROTATION, 3 * MAX_ROTATION));
        shieldRb.AddTorque(3 * MAX_ROTATION, 2 * MAX_ROTATION, MAX_ROTATION);
    }
}