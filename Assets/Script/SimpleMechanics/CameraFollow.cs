﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {
    public Transform playerTransform;

    private Vector3 camOffset;

    [Range(0.01f, 1.0f)]
    public float smoothFactor = 0.5f;
    // Use this for initialization
    void Start() {
        camOffset = transform.position - playerTransform.position;
    }

    // LateUpdate is called after Update method
    void FixedUpdate() {
        Vector3 newPos = playerTransform.position + camOffset;
        transform.position = Vector3.Slerp(transform.position, newPos, smoothFactor);
    }
}
