﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sway : MonoBehaviour {
    private Transform playerTransform;
    private Rigidbody rb;
    private float pushForce;
    private readonly float maxForce = 20f;

    // Start is called before the first frame update
    void Start() {
        playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
        rb = GetComponent<Rigidbody>();
        Random.InitState(System.DateTime.Now.Millisecond);
        pushForce = Random.Range(-maxForce * rb.mass, maxForce * rb.mass);
    }

    // Update is called once per frame
    void Update() {
        if (transform.position.z - playerTransform.position.z < 30 && transform.position.z - playerTransform.position.z > 10) {
            rb.AddForce(new Vector3(pushForce, 0, 0));
        }
        Restrain();
    }

    void Restrain() {
        if (rb.position.x > 12.5f) {
            rb.position = new Vector3(12.5f, rb.position.y, rb.position.z);
        }
        if (rb.position.x < -12.5f) {
            rb.position = new Vector3(-12.5f, rb.position.y, rb.position.z);
        }
    }
}
