﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Prologue : MonoBehaviour {
    public AudioSource source;
    public AudioClip welcome;
    public static int toggle;

    // Use this for initialization
    void Start() {
        source.PlayOneShot(welcome);
    }

    public void ToMenu() {
        SceneManager.LoadSceneAsync("MenuScene");
    }
}
