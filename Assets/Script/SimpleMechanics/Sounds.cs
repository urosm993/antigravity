﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sounds : MonoBehaviour {
    public AudioSource audioSource;
    public AudioSource shield;
    public AudioSource lumina;
    public AudioSource metalHit;
    // Use this for initialization
    void Start() {
    }

    void OnCollisionEnter(Collision collision) {
        if (collision.gameObject.CompareTag("Collectable")) {
            shield.Play();
        }
        if (collision.gameObject.CompareTag("Lumina")) {
            lumina.Play();
        }
        if (collision.gameObject.CompareTag("Obsticle")) {
            if (!metalHit.isPlaying)
                metalHit.Play();
        }
    }
    // Update is called once per frame
    void Update() {
        if (!audioSource.isPlaying) {
            audioSource.Play();
        }
    }
}
