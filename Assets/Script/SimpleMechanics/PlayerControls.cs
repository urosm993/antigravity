﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControls : MonoBehaviour {
    public GameObject shield;
    private Rigidbody rb;
    public float drift;
    public float speed;
    public float rotation;
    private Rigidbody shieldRb;
    private Renderer shieldRenderer;

    private readonly float ROT = 0.005f;

    void Start() {
        rb = GetComponent<Rigidbody>();
        rb.AddTorque(new Vector3(rotation, 2 * rotation, 3 * rotation));
        rb.maxAngularVelocity = 10;
        shieldRb = shield.GetComponent<Rigidbody>();
        if (ScorePreservation.level >= 18 && ScorePreservation.level <= 23) {
            speed *= 1.6f;
            rotation *= 1.8f;
            drift *= 1.6f;
        }
        if (ScorePreservation.level >= 24 && ScorePreservation.level <= 29) {
            speed *= 1.2f;
            rotation *= 1.2f;
            drift *= 1.2f;
        }
    }

    // Update is called once per frame
    void FixedUpdate() {
        rb.AddForce(new Vector3(0, 0, speed));
        shieldRb.AddTorque(3 * rotation, 2 * rotation, rotation, ForceMode.Impulse);

        if (Input.touches.Length > 0 && rb.position.y == 0) {
            Touch t = Input.touches[0];
            if (t.position.x - Screen.width / 2 > 0) {
                rb.AddTorque(new Vector3(ROT, 2*ROT, 3*ROT), ForceMode.Impulse);
                rb.AddForce(new Vector3(drift, 0, 0), ForceMode.Impulse);
            } else {
                rb.AddTorque(new Vector3(2*ROT, 3*ROT, ROT), ForceMode.Impulse);
                rb.AddForce(new Vector3(-drift, 0, 0), ForceMode.Impulse);
            }
        }
        Keyboard();
    }

    void Keyboard() {
        if (Input.GetKey("left")) {
            rb.AddForce(new Vector3(-drift, 0, 0), ForceMode.Impulse);
        }
        if (Input.GetKey("right")) {
            rb.AddForce(new Vector3(drift, 0, 0), ForceMode.Impulse);
        }
    }

}
