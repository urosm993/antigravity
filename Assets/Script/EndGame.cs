﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using UnityEngine.Advertisements;


public class EndGame : MonoBehaviour {
    public Text scoreText;
    public Text bonus;
    public Text progressText;
    public Text luminasText;
    public Text message;
    public Text bestText;
    public Text levelStats;
    private int score;
    private int progress;
    private readonly int LUM_SCORE_FACT = 180;

    public Button adButton;
    public GameObject cubieEndless;
    public GameObject liveCanvas;
    public GameObject pausedCanvas;
    private int best;

    // Use this for initialization
    void Awake() {
        int lvl = ScorePreservation.level;
        UnityEngine.Random.InitState(DateTime.Now.Millisecond);
        if (lvl > -1) {
            ShortAd();
            score = ScorePreservation.currentScore;
            progress = ScorePreservation.currentProgress;
            scoreText.text = score.ToString();
            progressText.text = progress.ToString() + '%';

            if (lvl == 5 || lvl == 11 || lvl == 17 || lvl == 23 || lvl == 29) {
                ScorePreservation.totalLuminas = ScorePreservation.collectedLuminas;
                luminasText.text = "\t\t" + ScorePreservation.totalLuminas + " luminas left";
            } else if (lvl < 2) {
                luminasText.text = "";
            } else {
                int acquiredLuminas = 0;
                acquiredLuminas += ScorePreservation.collectedLuminas;
                acquiredLuminas += score / LUM_SCORE_FACT;
                int levelCompletedBonus = 0;
                if (progress >= 99) {
                    levelCompletedBonus = (int)(ScorePreservation.collectedLuminas * 0.5f);
                }
                acquiredLuminas += levelCompletedBonus;
                luminasText.text = "+ " + ScorePreservation.collectedLuminas + " luminas (collected)\n" +
                                   "+ " + levelCompletedBonus + " luminas (level completed)\n" +
                                   "+ " + score / LUM_SCORE_FACT + " luminas (score)\n" +
                    "   " + acquiredLuminas + " total acquired";

                ScorePreservation.totalLuminas += acquiredLuminas;
            }
            if (progress >= 99) {
                bonus.text = "Difficulty bonus: " + (int)((score / ScorePreservation.DIFF_BONUS) * ScorePreservation.levelsDifficulty[ScorePreservation.level]);
                if (lvl < 29) {
                    message.text = "Well done!";
                } else {
                    message.text = "TO BE CONTINUED...";
                }
            } else {
                message.text = "I know you can do better!";
            }

            if (progress >= 99 && lvl < 29 && (lvl + 1 == ScorePreservation.maximumLevel)) {
                ScorePreservation.levelsDifficulty[lvl + 1] = (float)Math.Round(ScorePreservation.levelsDifficulty[lvl] + 0.15, 2);
            }

            if (progress < 99 && (lvl == ScorePreservation.maximumLevel)) {
                // Completely magic formula
                ScorePreservation.levelsDifficulty[lvl] = (float)Math.Round(ScorePreservation.levelsDifficulty[lvl] - (ScorePreservation.levelsDifficulty[lvl] * (0.9f - progress / 70f) * 0.1f), 2);
                if (ScorePreservation.levelsDifficulty[lvl] < 0.5f) {
                    ScorePreservation.levelsDifficulty[lvl] = 0.5f;
                }
            }
            SaveGame();
        }
    }

    public void GameOver() {
        bool adsReady = Ads.getAds().IsReady();
        adButton.interactable = adsReady;
        liveCanvas.gameObject.SetActive(false);
        pausedCanvas.gameObject.SetActive(true);
        score = ScorePreservation.currentScore;
        best = ScorePreservation.bestScore;

        scoreText.text = score.ToString();
        bestText.text = best.ToString();
        message.text = "+" + ScorePreservation.collectedLuminas + " luminas";
        ScorePreservation.totalLuminas += ScorePreservation.collectedLuminas;

        if (score >= best) {
            //GoogleServices.UpdateScoreEndless(score);
            ScorePreservation.bestScore = score;
        }
        SaveGame();
    }

    // Update is called once per frame
    public void Continue() {
        if (ScorePreservation.level == -1) {
            adButton.interactable = true;
        }
        if (ScorePreservation.level == 29 && progress >= 99) {
            SceneManager.LoadScene("Credits");
        } else {
            SceneManager.LoadScene("MenuScene");
        }
    }

    private void ShortAd() {
        if (UnityEngine.Random.Range(0, 100) < 15) {
            if (Ads.getAds().IsReady()) {
                Ads.getAds().PlayShortAd();
            }
        }
    }

    public void PlayAd() {
        ShowOptions so = new ShowOptions {
            resultCallback = DemandAd
        };
        Advertisement.Show("rewardedVideo", so);
    }

    private void DemandAd(ShowResult sr) {
        if (sr == ShowResult.Finished) {
            cubieEndless.SetActive(true);
            liveCanvas.gameObject.SetActive(true);
            pausedCanvas.gameObject.SetActive(false);
            adButton.interactable = false;
        }
    }

    void SaveGame() {
        if (ScorePreservation.level == -1) {
            PlayerPrefs.SetInt("bestScore", ScorePreservation.bestScore);
            PlayerPrefs.SetInt("luminas", ScorePreservation.totalLuminas);
        } else {
            PlayerPrefs.SetInt("level", ScorePreservation.maximumLevel);
            PlayerPrefs.SetInt("luminas", ScorePreservation.totalLuminas);
            int total = PlayerPrefs.GetInt("totalScore");
            PlayerPrefs.SetInt("totalScore", total + ScorePreservation.addScore);
            //GoogleServices.UpdateScoreStory(total + ScorePreservation.addScore);
            ScorePreservation.addScore = 0;

            for (int i = 0; i < ScorePreservation.levelsScore.Length; i++) {
                PlayerPrefs.SetInt("score" + i, ScorePreservation.levelsScore[i]);
                PlayerPrefs.SetInt("progress" + i, ScorePreservation.levelsProgress[i]);
                PlayerPrefs.SetFloat("difficulty" + i, ScorePreservation.levelsDifficulty[i]);
            }
        }
    }

}
