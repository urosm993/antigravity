﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelSetup : MonoBehaviour {
    public Transform playerTransform;
    public Transform obstacle;
    public Transform obstacleSway;
    public Transform attractive;
    public Transform repellent;
    public Transform collectable;
    public Transform lumina;
    public Transform meteor;
    private LevelData lvlData;
    private int lvl;

    public static LevelData[] levels = new LevelData[30];

    void Awake() {
        //PROLOGUE
        levels[0] = new LevelData(0, 25, 0, 0, 0, 2);
        levels[1] = new LevelData(1, 25);
        levels[2] = new LevelData(2, 30);
        levels[3] = new LevelData(3, 40);
        levels[4] = new LevelData(4, 45);
        levels[5] = new LevelData(5, 30);

        //DETERMINATION
        levels[6] = new LevelData(6, 35);
        levels[7] = new LevelData(7, 37);
        levels[8] = new LevelData(8, 41);
        levels[9] = new LevelData(9, 43);
        levels[10] = new LevelData(10, 45);
        levels[11] = new LevelData(11, 30);

        //INCOVENIENCE (Meteors)
        levels[12] = new LevelData(12, 0, 10, 0, 0, 3);
        levels[13] = new LevelData(13, 0, 14);
        levels[14] = new LevelData(14, 0, 18);
        levels[15] = new LevelData(15, 0, 20);
        levels[16] = new LevelData(16, 0, 22);
        levels[17] = new LevelData(17, 0, 16);

        //RISK (Fast)
        levels[18] = new LevelData(18, 20);
        levels[19] = new LevelData(19, 23);
        levels[20] = new LevelData(20, 25);
        levels[21] = new LevelData(21, 30);
        levels[22] = new LevelData(22, 35);
        levels[23] = new LevelData(23, 25);

        //WORTH (Magnets)
        levels[24] = new LevelData(24, 20, 0, 15, 0);
        levels[25] = new LevelData(25, 20, 0, 25, 0);
        levels[26] = new LevelData(26, 20, 0, 0, 30);
        levels[27] = new LevelData(27, 20, 0, 20, 25);
        levels[28] = new LevelData(28, 20, 0, 40, 45);
        levels[29] = new LevelData(29, 20, 0, 20, 25);

        lvl = ScorePreservation.level;
        lvlData = levels[lvl];
    }

    void Start() {
        float difficulty = ScorePreservation.levelsDifficulty[lvl];
        lvlData.Obstacles = (int)(lvlData.Obstacles * difficulty);
        lvlData.MeteorsFeq = (int)(lvlData.MeteorsFeq * difficulty);
        lvlData.Att = (int)(lvlData.Att * difficulty);
        lvlData.Repel = (int)(lvlData.Repel * difficulty);

        Random.InitState(System.DateTime.Now.Millisecond);
        Physics.gravity = new Vector3(0, -100, 0);
        SpawnObjects();
        InvokeRepeating("SpawnMeteors", 10f, 0.5f);
    }

    void SpawnCollectables(int k) {
        for (int i = 0; i < lvlData.Collectables;) {
            float rndWidth = Random.Range(-lvlData.WidthLimit, lvlData.WidthLimit);
            float rndZpos = k * lvlData.SegLen + Random.Range(0, lvlData.SegLen);
            if (Physics.CheckSphere(new Vector3(rndWidth, 0, rndZpos), 2)) {
                continue;
            }
            Instantiate(collectable, new Vector3(rndWidth, 0, rndZpos), Quaternion.identity);
            i++;
        }
    }

    void SpawnMeteors() {
        if (Random.Range(0, 100) < lvlData.MeteorsFeq) {
            Instantiate(meteor, new Vector3(30, 0, playerTransform.position.z + Random.Range(150, 500)), Quaternion.identity);
            Instantiate(meteor, new Vector3(30, 0, playerTransform.position.z + Random.Range(150, 500)), Quaternion.identity);
            Instantiate(meteor, new Vector3(30, 0, playerTransform.position.z + Random.Range(150, 500)), Quaternion.identity);
            Instantiate(meteor, new Vector3(30, 0, playerTransform.position.z + Random.Range(150, 500)), Quaternion.identity);
        }
    }

    void SpawnLuminas(int k) {
        if (lvl < 2) {
            return;
        }
        int variance = Random.Range(-1, 3);
        for (int i = 0; i < lvlData.Luminas + variance;) {
            float rndWidth = Random.Range(-lvlData.WidthLimit, lvlData.WidthLimit);
            float rndZpos = k * lvlData.SegLen / 2 + Random.Range(-lvlData.SegLen / 2, lvlData.SegLen / 2);
            if (Physics.CheckSphere(new Vector3(rndWidth, 0, rndZpos), 1)) {
                continue;
            }
            Instantiate(lumina, new Vector3(rndWidth, 0, rndZpos), Quaternion.identity);
            i++;
        }
    }

    void SpawnObjects() {
        float obsticleNo = lvlData.Obstacles;
        for (int k = 0; k < lvlData.Segments; k++) {
            SpawnCollectables(k);
            SpawnLuminas(k);

            int done = 0;
            int failed = 0;
            int j = 0;
            while (j < obsticleNo) {
                float rndWidth = Random.Range(-lvlData.WidthLimit, lvlData.WidthLimit);
                float rndZpos = k * lvlData.SegLen + Random.Range(0, lvlData.SegLen - lvlData.BufferZone);
                if (Physics.CheckSphere(new Vector3(rndWidth, 0, rndZpos), 8)) {
                    failed++;
                    if (failed > 200) {
                        failed = 0;
                        j++;
                    }
                } else {
                    if (lvl >= 6 && lvl <= 11) {
                        if (Random.Range(0, 1f) > 0.8f) {
                            Instantiate(obstacleSway, new Vector3(rndWidth, -97, rndZpos), Quaternion.identity);
                        } else {
                            Instantiate(obstacle, new Vector3(rndWidth, -97, rndZpos), Quaternion.identity);
                        }
                    } else if (lvl >= 24) {
                        if (Random.Range(0, 100) < lvlData.Att) {
                            Instantiate(attractive, new Vector3(rndWidth, -97, rndZpos), Quaternion.identity);
                        } else if (Random.Range(0, 100) < lvlData.Repel) {
                            Instantiate(repellent, new Vector3(rndWidth, -97, rndZpos), Quaternion.identity);
                        } else {
                            Instantiate(obstacle, new Vector3(rndWidth, -97, rndZpos), Quaternion.identity);
                        }
                    } else {
                        Instantiate(obstacle, new Vector3(rndWidth, -97, rndZpos), Quaternion.identity);
                    }
                    j++;
                    done++;
                }
            }
            //Debug.Log("Attempted:" + obsticleNo + " Done:" + done + " Seg:" + k);
            obsticleNo += lvlData.Increase;
        }
    }

    // Update is called once per frame
    void FixedUpdate() {
        if (playerTransform.position.z > lvlData.Segments * lvlData.SegLen) {
            SceneManager.LoadScene("ScoreScene");
        }
        if (playerTransform.position.y < lvlData.RockBottom) {
            SceneManager.LoadScene("ScoreScene");
        }
        RestrainXPosition();
    }

    void RestrainXPosition() {
        if (playerTransform.position.x > lvlData.WidthLimit) {
            playerTransform.position = new Vector3(lvlData.WidthLimit, playerTransform.position.y, playerTransform.position.z);
        }
        if (playerTransform.position.x < -lvlData.WidthLimit) {
            playerTransform.position = new Vector3(-lvlData.WidthLimit, playerTransform.position.y, playerTransform.position.z);
        }
    }

    public int GetTotalLevelLen() {
        return lvlData.GetTotalLevelLen();
    }
}
