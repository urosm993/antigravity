﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Advertisements;

public class MainMenu : MonoBehaviour {
    public Text[] outOfFive;
    public Text[] chapterNames;
    public Button[] chapterButtons;
    public Button[] levelButtons;
    public Button[] rights;
    public Button[] adButtons;

    public Text[] difficulty;
    public GameObject cubbieMenu;

    public Text totalLuminasStory;
    public Text totalLuminasEndless;
    public Text totalLuminasShop;
    public Text totalLuminasUpgrade;
    public Text maxLevelText;
    public Text totalScoreText;
    public Text note;
    public Text endlessScore;
    private int maxLevel;
    private int levelToLoad = 0;

    void Awake() {
        //Advertisement
        Advertisement.Initialize("3412184");
        Random.InitState(System.DateTime.Now.Millisecond);
        LoadPrefs();
        StoryMode();
        EndlessPage();

        InvokeRepeating("UpdateLuminas", 2f, 2f);
        InvokeRepeating("CheckForAds", 10f, 10f);
    }

    public void StartLevel() {
        if (levelToLoad == 0) {
            cubbieMenu.SetActive(false);
        }
        StartCoroutine(LoadYourAsyncScene(levelToLoad));
    }

    private void LoadPrefs() {
        if (PlayerPrefs.GetInt("First") == 0) {
            for (int j = 0; j < ScorePreservation.levelsDifficulty.Length; j++) {
                PlayerPrefs.SetFloat("difficulty" + j, 1.0f);
                //ScorePreservation.playerId = Random.Range(100000, 999999);
                //PlayerPrefs.SetFloat("playerId", ScorePreservation.playerId);
                ScorePreservation.levelsDifficulty[j] = 1;
            }
            PlayerPrefs.SetInt("First", 1);
        }

        //ScorePreservation.playerId = PlayerPrefs.GetFloat("playerId");
        ScorePreservation.maximumLevel = PlayerPrefs.GetInt("level");
        ScorePreservation.totalLuminas = PlayerPrefs.GetInt("luminas");
        ScorePreservation.totalScore = PlayerPrefs.GetInt("totalScore");
        ScorePreservation.bestScore = PlayerPrefs.GetInt("bestScore");

        for (int i = 0; i < ScorePreservation.levelsScore.Length; i++) {
            ScorePreservation.levelsScore[i] = PlayerPrefs.GetInt("score" + i);
            ScorePreservation.levelsProgress[i] = PlayerPrefs.GetInt("progress" + i);
            ScorePreservation.levelsDifficulty[i] = PlayerPrefs.GetFloat("difficulty" + i);
        }

        PlayerUnlocks.startShield = PlayerPrefs.GetInt("startShield");
        PlayerUnlocks.superShield = PlayerPrefs.GetInt("superShield");
        PlayerUnlocks.spawnShields = PlayerPrefs.GetInt("spawnShields");
        PlayerUnlocks.repair = PlayerPrefs.GetInt("repair");
        PlayerUnlocks.luminasTimes2 = PlayerPrefs.GetInt("luminasTimes2");
        PlayerUnlocks.luminasWorthMore = PlayerPrefs.GetInt("luminasWorthMore");

        PlayerUnlocks.goldenCubie = PlayerPrefs.GetInt("goldenCubie");
        PlayerUnlocks.iceCubie = PlayerPrefs.GetInt("iceCubie");
        PlayerUnlocks.goldenShield = PlayerPrefs.GetInt("goldenShield");

        PlayerUnlocks.cosmeticsSelected = PlayerPrefs.GetInt("cosmeticsSelected");
        PlayerUnlocks.shieldSelected = PlayerPrefs.GetInt("shieldSelected");
    }

    public void LevelsButton() {
        maxLevel = ScorePreservation.maximumLevel;
        for (int i = 0; i <= maxLevel; i++) {
            levelButtons[i].interactable = true;
            levelButtons[i].GetComponentInChildren<Text>().color = new Color(0f, 0.8627451f, 0.8627451f, 1);
        }
        for (int i = maxLevel + 1; i < levelButtons.Length; i++) {
            levelButtons[i].interactable = false;
            levelButtons[i].GetComponentInChildren<Text>().color = new Color(0.2352941f, 0.2352941f, 0.2352941f, 1);
        }

        if (maxLevel > 5) {
            rights[0].interactable = true;
            rights[0].GetComponentInChildren<Text>().color = new Color(0f, 0.8627451f, 0.8627451f, 1);
        } else {
            rights[0].interactable = false;
            rights[0].GetComponentInChildren<Text>().color = new Color(0.2352941f, 0.2352941f, 0.2352941f, 1);
        }
        if (maxLevel > 11) {
            rights[1].interactable = true;
            rights[1].GetComponentInChildren<Text>().color = new Color(0f, 0.8627451f, 0.8627451f, 1);
        } else {
            rights[1].interactable = false;
            rights[1].GetComponentInChildren<Text>().color = new Color(0.2352941f, 0.2352941f, 0.2352941f, 1);
        }
        if (maxLevel > 17) {
            rights[2].interactable = true;
            rights[2].GetComponentInChildren<Text>().color = new Color(0f, 0.8627451f, 0.8627451f, 1);
        } else {
            rights[2].interactable = false;
            rights[2].GetComponentInChildren<Text>().color = new Color(0.2352941f, 0.2352941f, 0.2352941f, 1);
        }
        if (maxLevel > 23) {
            rights[3].interactable = true;
            rights[3].GetComponentInChildren<Text>().color = new Color(0f, 0.8627451f, 0.8627451f, 1);
        } else {
            rights[3].interactable = false;
            rights[3].GetComponentInChildren<Text>().color = new Color(0.2352941f, 0.2352941f, 0.2352941f, 1);
        }
    }
    public void LoadLevel(int level) {
        note.text = stories[level];
        levelToLoad = level;
        if (level != 0) {
            cubbieMenu.SetActive(false);
        }
    }

    private void CheckForAds() {
        bool inter = false;
        if (Internet.Connected()) {
            if (Ads.getAds().IsReady()) {
                inter = true;
            }
        }

        foreach (Button b in adButtons) {
            b.interactable = inter;
        }
    }

    IEnumerator LoadYourAsyncScene(int level) {
        ScorePreservation.level = level;
        AsyncOperation asyncLoad;
        if (level == 5 || level == 11 || level == 17 || level == 23 || level == 29) {
            asyncLoad = SceneManager.LoadSceneAsync("LevelDarkness");
        } else {
            asyncLoad = SceneManager.LoadSceneAsync("LevelStandard");
        }
        // Wait until the asynchronous scene fully loads
        while (!asyncLoad.isDone) {
            yield return null;
        }
    }

    public void StoryMode() {
        CheckForAds();
        UpdateLuminas();
        maxLevelText.text = ScorePreservation.maximumLevel + "/30";
        totalScoreText.text = ScorePreservation.totalScore + "";
        int cnt = ScorePreservation.maximumLevel;
        int i = 0;
        while (cnt >= 6) {
            cnt -= 6;
            outOfFive[i].text = "6/6";
            chapterButtons[i].interactable = true;
            outOfFive[i].color = new Color(0.3137255f, 0.9411765f, 0.8627451f, 1);
            chapterNames[i].GetComponentInChildren<Text>().color = new Color(0f, 0.8627451f, 0.8627451f, 1);

            i++;
        }
        if (i < outOfFive.Length) {
            outOfFive[i].text = cnt + "/6";
            chapterButtons[i].interactable = true;
            outOfFive[i].color = new Color(0.3137255f, 0.9411765f, 0.8627451f, 1);
            chapterNames[i].GetComponentInChildren<Text>().color = new Color(0f, 0.8627451f, 0.8627451f, 1);
            i++;
        }
        while (i < outOfFive.Length) {
            outOfFive[i].text = "0/6";
            chapterButtons[i].interactable = false;
            outOfFive[i].color = new Color(0.2352941f, 0.2352941f, 0.2352941f, 1);
            chapterNames[i].GetComponentInChildren<Text>().color = new Color(0.2352941f, 0.2352941f, 0.2352941f, 1);
            i++;
        }
    }

    public void EndlessPage() {
        CheckForAds();
        UpdateLuminas();
        endlessScore.text = ScorePreservation.bestScore + "";
    }

    public void EndlessButton() {
        StartCoroutine(LoadEndlessAsync());
    }

    IEnumerator LoadEndlessAsync() {
        ScorePreservation.level = -1;
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("LevelEndless");
        while (!asyncLoad.isDone) {
            yield return null;
        }
    }

    public void PlayAd() {
        Ads.getAds().PlayAd();
    }

    private void UpdateLuminas() {
        totalLuminasStory.text = ScorePreservation.totalLuminas + "";
        totalLuminasEndless.text = ScorePreservation.totalLuminas + "";
        totalLuminasShop.text = ScorePreservation.totalLuminas + "";
        totalLuminasUpgrade.text = ScorePreservation.totalLuminas + "";
    }

    public void DifficultyMenu() {
        for (int k = 0; k < 30; k++) {
            difficulty[k].text = k + ": " + ScorePreservation.levelsDifficulty[k];
        }
    }

    public void UnlockAll() {
        ScorePreservation.maximumLevel = 29;
        ScorePreservation.totalLuminas = 1000;
    }

    public void Credits() {
        SceneManager.LoadSceneAsync("Credits");
    }

    public void ProgressReset() {
        PlayerPrefs.DeleteAll();
        ScorePreservation.maximumLevel = 0;
        Application.Quit();
    }

    public void ExitButton() {
        Application.Quit();
    }

    private readonly string[] stories = {
        "Hi. I'm Cubbie.\nYou will be my navigator through the Galaxy.\nJust try to avoid the big pillars, okay?\nLet's go!",
        "Things are about to get much more difficult.\nMany navigators fail this level.\nNo pressure.",
        "You seem to be doing pretty well!\nNow you'll see tiny red things flying around.\nThose are called luminas,\nwe need them!",
        "Luminas are precious but difficult to catch.\nPlease don't throw me into the abyss while chasing them.",
        "Someone told me that this game runs a dynamic difficulty algorithm that uses mathematical formulas to ensure...\nWho cares, doesn't matter.",
        "When darkness comes I can use luminas to illuminate the way.\nIf we run out of them we'll end up in complete darkness.",

        "Congratulations on passing the prologue!\nNow we're talking!\nTo be honest, you didn't seem like a good choice at first.",
        "What!? They are moving now!\nI trust your senses.",
        "Details matter.",
        "Have you noticed that pillars move just to one side each level?\nThat could help us!",
        "While you were navigating around the obstacles,\nI took some time to think...\nDo you believe there is a meaning to this?",
        "Ok, I'll let you concentrate now.\nInto the darkness we go.",

        "If you feel this is a little bit repetitive,\nyou don't want to hear about some people's lives.",
        "Woah! Huge rocks are flying.\nDon't get me crushed.",
        "Did you know I'm actually yellow?\nI appear light blue when I'm anxious.",
        "I think there is a meaning.",
        "Tell me,\ndo you ever overthink?",
        "No more talking.\nBlazing rocks are flying.",

        "How fast is too fast?",
        "Dear navigator,\nIf you are reading this for the 30th time already,\nI'd suggest you lie down for a bit.",
        "This will really test your reflexes.",
        "You know,\nsometimes I love flying fast.\nBut It's scary that you can miss so many things.",
        "\"It's not only the scenery you miss, you can also miss the sense of where you are going and why.\" said someone, and I agree with it.",
        "Have you tried running blindfolded?",

        "The things that attract you can sometimes be harmful.",
        "Most of the navigators usually give up by now so I forgot my script for this part.",
        "Just tell me the truth.",
        "I never feel stressed.",
        "Creation is easy.",
        "If we are always moving forward\n and the future cannot be seen,\n how do we find the strength\n to confront it?",
        //courage and validation.
        
     /*
     private readonly string[] stories = {
        "Around 7% of players can't pass level 0.\nNo pressure.",
        "If you are reading this,\nyou obviously understood the controls.\nHere is where the game actually starts.",
        "You'll see tiny red things flying around.\nThose are called luminas.\nEveryone wants them!",
        "Luminas are precious but difficult to catch.\nDon't die chasing them.",
        "The game runs dynamic difficulty algorithm that uses mathematical formulas to ensure...\n Who cares, doesn't matter.",
        "When darkness comes luminas keep your path illuminated!\nIf you run out of luminas you'll enter complete darkness.",

        "Congratulations on passing the prologue!\nNow we're talking!",
        "Is there a meaning to this game?",
        "Details matter.",
        "Hint: Obstacles move just to one side each level.",
        "Do you ever overthink?",
        "Ok, I'll let you concentrate now.",

        "If you feel this game is a little repetitive,\nyou don't want to know about people's lives.",
        "If see this note for more than 30 times,\nI'd suggest you lie down for a bit",
        "Just tell me the truth.",
        "The Cube changed colour slightly from the beginning.\nDid you notice that?",
        "There is a meaning.",
        "No more talking. Blazing rocks are flying.",

        "How fast is too fast?",
        "\"It's not only the scenery you miss, you can also miss the sense of where you are going and why.\"",
        "This can really test your reflexes.",
        "I'll become ill if you remove my apostrophe.\n Get it?",
        "We have art in order not to die of the truth.",
        "If we are always moving forward\n and the future cannot be seen,\n how do we find the strength\n to confront it?",

        "The things that attract you can sometimes be harmful.",
        "Let me tell you a short story...",
        "She never felt stressed.",
        "Creation is easy.",
        "I handle social situations with ease.", //Move down
        "This is not scary.", //Move down
        //courage and validation.

        //Next up!
        "This is not scary"
        "Just tell me the truth.",
        "This is not scary.", //Move down
        "I handle social situations with ease.", //Move down
       */
};

}
